# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-02-18 03:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0005_auto_20170218_0236'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='slack_user_id',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='topic',
            name='slack_channel_id',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
