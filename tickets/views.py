import json
import re
import requests

from django.http import HttpResponse
from django.conf import settings
from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

from django_slack import slack_message

from tickets.models import Ticket, Topic, Profile, Comment, CommentIntent


def index(request):
    return render(request, "index.html")


def webhook(from_user, to_user, text, ticket=None):
    url = "https://hooks.slack.com/services/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"
    if ticket:
        text = text + ": " + ticket.title
    data = {
        "username": from_user.username,
        "text": text,
        "channel": "@" + to_user.username,
        "attachments": ticket.as_slack()
    }
    response = requests.post(
        url, data=json.dumps(data),
        headers={'Content-Type': 'application/json'}
    )

def slack_simple_result(message, ephemeral, replace_original=None):
    if ephemeral:
        response_type = "ephemeral"
    else:
        response_type = "in_channel"
    response = {
        "response_type": response_type,
        "text": message
    }
    if replace_original is not None:
        response['replace_original'] = replace_original
    return HttpResponse(json.dumps(response), content_type="application/json")


def get_user_from_string(user, me=None):
    user = user.lower().strip()
    if me and user == "me":
        return me
    if user.startswith("@"):
        return User.objects.get_or_create(username=user[1:])[0]


def get_user_from_slack(user, me=None):
    profiles = Profile.objects.filter(
        Q(slack_user_name=user['name']) | Q(slack_user_id=user['id']))
    if not profiles:
        user_instance, _ = User.objects.get_or_create(username=user['name'])
        profile = Profile.objects.create(
            user=user_instance,
            slack_user_name=user['name'],
            slack_user_id=user['id'],
        )
    else:
        profile = profiles[0]
    return profile.user


def get_topic_from_slack(channel):
    if channel['name'] == "directmessage":
        return None
    else:
        topic = Topic.objects.get_or_create(name=channel['name'])[0]
        if not topic.slack_channel_id:
            topic.slack_channel_id = channel['id']
            topic.save()
        return topic


class Command():

    USER_SYNTAX = "(?P<user>me|@\w+)"
    PRIORITY_SYNTAX = "(?P<priority>fire|high|normal|low)"
    TEXT_SYNTAX = "(?P<text>\".+\"|“.+\")"

    def __init__(self, user, command, topic):
        self.user = user
        self.command = command.strip()
        self.topic = topic

        self.ephemeral = False
        self.attachments = []
        self.text = None

        self.mention = None
        self.priority = None
        self.string = None

        self.parse_command()


    def parse_command(self):
        self.parse_mention()
        self.parse_string()
        self.parse_priority()
        if self.command.startswith('open'):
            self.parse_open()
        elif self.command.startswith('help'):
            self.show_help()
        else:
            self.parse_show()

    def show_error(self, message):
        self.ephemeral = True
        self.text = message

    def show_help(self):
        self.attachments.append({
            'title': '/ticket',
            'text': 'Show open tickets (only visible to you)'})
        self.attachments.append({
            'title': '/ticket open',
            'text': 'Open new ticket (share with others)'})
        self.attachments.append({
            'title': 'Parametes',
            'text': ('Both commands support the same parameters, in any order:\n'
                     '1. _Text_ = "between quotes"\n'
                     '2. _User_ = me or @user\n'
                     '3. _Priority_: fire | high | normal | low'),
            "mrkdwn_in": ["text", "pretext"]
        })
        self.attachments.append({
            'title': 'Examples',
            'text': ('/ticket open "Pay bills" @johnsmith fire\n'
                     '/ticket @johnsmith "bills"'),
        })
        self.ephemeral = True

    def parse_mention(self):
        user = re.search(self.USER_SYNTAX, self.command)
        if user:
            user = user.group(1)
            self.mention = get_user_from_string(user, me=self.user)

    def parse_priority(self):
        priority = re.search(self.PRIORITY_SYNTAX, self.command)
        if priority:
            priority = priority.group(1)
            self.priority = Ticket.get_priority_from_text(priority)

    def parse_string(self):
        text = re.search(self.TEXT_SYNTAX, self.command)
        if text:
            self.string = text.group(1).replace('"', '')
            self.string = self.string.replace('“', '')

    def parse_open(self):
        if not self.string:
            self.show_help()
            return
        if not self.topic:
            self.text = "You can create tickets only in public channels"
            self.ephemeral = True
            return
        title = self.string[:100]
        description = self.string[100:]
        ticket = Ticket.objects.create(author=self.user, title=title, description=description, topic=self.topic)
        if self.priority:
            ticket.priority = self.priority
        if self.mention:
            ticket.assigned_to = self.mention
        ticket.save()
        self.attachments += ticket.as_slack()
        self.text = "New ticket"

    def parse_show(self):
        tickets = Ticket.objects.exclude(status=Ticket.CLOSED)
        if self.mention:
            tickets = tickets.filter(Q(author=self.mention)|Q(assigned_to=self.mention))
        if self.string:
            tickets = tickets.filter(title__icontains=self.string)
        if self.priority:
            tickets = tickets.filter(priority=self.priority)
        if self.topic:
            tickets = tickets.filter(topic=self.topic)
        if tickets:
            self.text = "Tickets found:"
            for ticket in tickets:
                self.attachments += ticket.as_slack(action="list")
        else:
            self.text = "No tickets to show"
        self.ephemeral = True

    def response_type(self):
        if self.ephemeral:
            return "ephemeral"
        else:
            return "in_channel"

    def response(self):
        return {
            "response_type": self.response_type(),
            "attachments": self.attachments,
            "text": self.text,
        }


@csrf_exempt
def slack(request):
    if 'ssl_check' in request.GET:
        return HttpResponse("Solo es un chequeo de slack")
    data = request.POST

    if not 'token' in data or data['token'] not in [settings.SLACK_TOKEN]:
        return slack_simple_result("Bad token", ephemeral=True)

    user = get_user_from_slack({'name': data['user_name'], 'id': data['user_id']})
    topic = get_topic_from_slack({'name': data['channel_name'], 'id': data['channel_id']})

    command = Command(user, data['text'], topic)
    return HttpResponse(json.dumps(command.response()), content_type="application/json")


@csrf_exempt
def button(request):
    data = json.loads(request.POST['payload'])
    token = data['token']
    if token != settings.SLACK_TOKEN:
        return slack_simple_result("Bad token", ephemeral=True)
    action = data['actions'][0]['name']
    value = data['actions'][0]['value']
    ticket_id = data['callback_id']
    user = get_user_from_slack(data['user'])
    topic = get_topic_from_slack(data['channel'])

    ticket = Ticket.objects.get(id=ticket_id)
    notify_to = [ticket.author]
    if ticket.assigned_to and ticket.assigned_to != ticket.author:
        notify_to.append(ticket.assigned_to)

    text = ""
    ephemeral = True
    replace_original = True
    if action == "open":
        ticket.status = Ticket.OPEN
        ticket.save()
        text = "Ticket reopened"
    elif action == "play":
        ticket.status = Ticket.DOING
        ticket.save()
        text = "Ticket Doing"
    elif action == "pause":
        ticket.status = Ticket.OPEN
        ticket.save()
        text = "Ticket Paused"
    elif action == "close":
        ticket.status = Ticket.CLOSED
        ticket.save()
        text = "Ticket Closed"
    elif action == "show":
        ephemeral = False
    elif action == "comment":
        if not topic:
            action = "comment_private"
        else:
            if value == "cancel":
                CommentIntent.objects.filter(user=user, topic=topic, ticket=ticket).update(pending=False)
                action = ""
            else:
                CommentIntent.objects.create(user=user, topic=topic, ticket=ticket)
    elif action == "priority":
        if value:
            ticket.priority = Ticket.get_priority_from_text(value)
            ticket.save()
            action = ""
            text = "Ticket priority changed to {}".format(ticket.get_priority_display())
    elif action == "assign":
        if value:
            ticket.assigned_to = get_user_from_string(value, me=user)
            ticket.save()
            action = ""
            text = "Ticket assigned to {}".format(ticket.assigned_to)
    elif action == "status":
        if value:
            ticket.status = int(value)
            ticket.save()
            action = ""
            text = "Ticket status changed to {}".format(ticket.get_status_display())

    if text:
        for to_user in notify_to:
            if to_user != user:
                webhook(user, to_user, text, ticket)

    if ephemeral:
        response_type = "ephemeral"
    else:
        response_type = "in_channel"
    response = {
        "response_type": response_type,
        "attachments": ticket.as_slack(action=action),
        "replace_original": True,
    }

    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def slack_news(request):
    data = json.loads(request.body.decode("utf-8"))
    if 'token' not in data or data['token'] != settings.SLACK_TOKEN:
        return HttpResponse("Bad token")
    if data['type'] == 'url_verification':
        return HttpResponse(data['challenge'])
    elif data['type'] == 'event_callback':
        # TODO: move to celery
        event = data['event']
        if event['type'] == "message" and "text" in event:
            try:
                user = User.objects.get(profile__slack_user_id=event['user'])
            except:
                user = None
            try:
                topic = Topic.objects.get(slack_channel_id=event['channel'])
            except:
                topic = None
            if 'thread_ts' in event:
                thread_ts = event['thread_ts']
            else:
                thread_ts = None
            comment = Comment.objects.create(
                slack_ts=event['ts'],
                thread_ts=thread_ts,
                user=user,
                topic=topic,
                text=event['text'],
            )
            comment.manage_for_tickets()
        return HttpResponse("")
