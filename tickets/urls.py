from django.conf.urls import url

from tickets.views import *


urlpatterns = [
    url(r'^slack_news', slack_news),
    url(r'^slack', slack),
    url(r'^button', button),
    url(r'^', index),
]
