import time

from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User
from django.template.loader import get_template


def ts_from_epoch(value):
    return int(time.mktime(value.timetuple()))


class Topic(models.Model):
    name = models.CharField(max_length=100)
    slack_channel_id = models.CharField(max_length=20, null=True, blank=True)
    members = models.ManyToManyField(User, blank=True, related_name="topics")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Ticket(models.Model):

    OPEN = 1
    THINKING = 2
    DOING = 3
    WAITING = 4
    CLOSED = 5
    STATUS_CODES = (
        (OPEN, 'Open'),
        (THINKING, 'Thinking'),
        (DOING, 'Doing'),
        (WAITING, 'Waiting'),
        (CLOSED, 'Closed'),
    )


    PRIORITY_CODES = (
        (1, 'Fire'),
        (2, 'High'),
        (3, 'Normal'),
        (4, 'Low'),
    )

    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, related_name="creates")
    assigned_to = models.ForeignKey(
        User, null=True, blank=True, related_name="assigned")
    status = models.IntegerField(default=1, choices=STATUS_CODES)
    priority = models.IntegerField(default=3, choices=PRIORITY_CODES)
    topic = models.ForeignKey(Topic, null=True, blank=True)

    class Meta:
        ordering = ('priority', '-status', 'created_at', 'title')

    @classmethod
    def get_priority_from_text(cls, text):
        text = text.lower().strip()
        if text == "fire":
            return 1
        if text == "high":
            return 2
        if text == "normal":
            return 3
        if text == "low":
            return 4

    def get_slack_actions(self, action=None):
        actions = []
        if action == "priority":
            actions = [
                {
                    "name": "priority",
                    "value": "fire",
                    "text": "Fire",
                    "type": "button",
                    "style": "danger",
                    "confirm": {
                        "title": "Are you sure to activate an emergency?",
                        "ok_text": "Yes",
                        "dismiss_text": "No"
                    }
                },
                {
                    "name": "priority",
                    "value": "high",
                    "text": "High",
                    "type": "button",
                },
                {
                    "name": "priority",
                    "value": "normal",
                    "text": "Normal",
                    "type": "button",
                },
                {
                    "name": "priority",
                    "value": "low",
                    "text": "Low",
                    "type": "button",
                },
            ]
        elif action == "status":
            for status in Ticket.STATUS_CODES:
                actions.append({
                    "name": "status",
                    "value": status[0],
                    "text": status[1],
                    "type": "button",
                })
        elif action == "list" and False:
            actions.append({
                "name": "show",
                "text": "Open",
                "type": "button",
            })
        elif action == "assign":
            actions.append({
                "name": "assign",
                "text": "me",
                "value": "me",
                "type": "button",
            })
            users = User.objects.filter(topics=self.topic).order_by('username')
            for u in users:
                actions.append({
                    "name": "assign",
                    "text": "@" + u.username,
                    "value": "@" + u.username,
                    "type": "button",
                })
        elif self.status == Ticket.CLOSED:
            actions = [
                {
                    "name": "open",
                    "text": "Reopen",
                    "type": "button",
                }
            ]
        else:
            actions = [
                {
                    "name": "assign",
                    "text": ":bust_in_silhouette:",
                    "type": "button",
                },
                {
                    "name": "priority",
                    "text": ":clock3:",
                    "type": "button",
                },
                {
                    "name": "comment",
                    "text": ":speech_balloon:",
                    "type": "button",
                }]
            if self.status == Ticket.OPEN:
                actions.append({
                    "name": "play",
                    "text": ":arrow_forward:",
                    "type": "button",
                })
            else:
                actions.extend([{
                    "name": "pause",
                    "text": ":double_vertical_bar:",
                    "type": "button",
                },
                {
                    "name": "close",
                    "text": ":black_square_for_stop:",
                    "type": "button",
                }])
            
        return actions

    def color_priority(self):
        return ['danger', 'warning', 'good', ''][self.priority - 1]

    def slack_help_texts(self, action):
        result = []
        if action == "comment_private":
            result.append({'text': ":no_entry: You can comment only in public channels"})
        if action == "comment":
            for comment in self.comments.all():
                result.append({
                    'text': comment.text,
                    "footer": comment.user.username,
                    "footer_icon": comment.user.profile.slack_photo_small,
                    "ts": ts_from_epoch(comment.timestamp),
                })
            result.append({
                'title': "Please, introduce your comment",
                'actions': [{
                    "name": "comment",
                    "value": "cancel",
                    "text": "Cancel",
                    "type": "button",
                }],
            })
        return result

    def as_slack(self, action=None):
        result = []
        actions = self.get_slack_actions(action)
        # text = "`{}` on `#{}`".format(self.get_status_display(), self.topic)
        text = ""
        if self.description:
            text = self.description + "\n" + text
        main = {
            "ts": ts_from_epoch(self.created_at),
            'title': self.title,
            'text': text,
            'callback_id': str(self.id),
            "fallback": "Ticket",
            "color": self.color_priority(),
            "actions": actions[:5],
            "mrkdwn": True,
            "mrkdwn_in": ['text'],
            "footer": self.author.username,
            "footer_icon": self.author.profile.slack_photo_small,
        }
        if self.assigned_to:
            main["thumb_url"] = self.assigned_to.profile.slack_photo_normal

        result.append(main)
        if len(actions) > 5:
            pos = 5
            while pos < len(actions):
                result.append({
                    'callback_id': str(self.id),
                    "fallback": "Can't show actions",
                    "actions": actions[pos:pos+5],
                    "color": self.color_priority(),
                })
                pos += 5
        for help_text in self.slack_help_texts(action):
            help_text.update({
                'callback_id': str(self.id),
                "color": self.color_priority(),
            })
            result.append(help_text)
        return result

    def __str__(self):
        return self.title


class Profile(models.Model):
    user = models.OneToOneField(User, related_name="profile")
    slack_user_name = models.CharField(max_length=100, null=True, blank=True)
    slack_user_id = models.CharField(max_length=20, null=True, blank=True)
    slack_photo_small = models.URLField(null=True, blank=True)
    slack_photo_normal = models.URLField(null=True, blank=True)
    slack_photo_large = models.URLField(null=True, blank=True)


class Comment(models.Model):
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, null=True, blank=True)
    topic = models.ForeignKey(Topic, null=True, blank=True)
    slack_ts = models.CharField(max_length=30, null=True, blank=True)
    thread_ts = models.CharField(max_length=30, null=True, blank=True)
    ticket = models.ForeignKey(Ticket, null=True, blank=True, related_name="comments")

    def manage_for_tickets(self):
        intents = CommentIntent.objects.filter(user=self.user, topic=self.topic, pending=True)
        if intents:
            self.ticket = intents[0].ticket
            self.save()
            intents.update(pending=False)


class CommentIntent(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, null=True, blank=True)
    topic = models.ForeignKey(Topic, null=True, blank=True)
    ticket = models.ForeignKey(Ticket, null=True, blank=True)
    pending = models.BooleanField(default=True)

    class Meta:
        ordering = ['-timestamp']