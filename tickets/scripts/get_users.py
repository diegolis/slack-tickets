import requests

from django.conf import settings
from django.contrib.auth.models import User
#from django.db.models.fields.related_descriptors import RelatedObjectDoesNotExist

from tickets.models import Profile


def run():
    url = "https://slack.com/api/users.list"
    response = requests.post(url, data={'token': settings.DIEGO_SLACK_TEST_TOKEN})
    data = response.json()
    if not data['ok']:
        print(data['error'])
    for member in data['members']:
        user, _ = User.objects.get_or_create(username=member['name'])
        try:
            profile = user.profile
        except:
            profile = Profile.objects.create(user=user, slack_user_id=member['id'], slack_user_name=member['name'])

        if 'profile' in member:
            if 'image_24' in member['profile']:
                profile.slack_photo_small = member['profile']['image_24']
                profile.slack_photo_normal = member['profile']['image_48']
                profile.slack_photo_large = member['profile']['image_192']
                profile.save()
