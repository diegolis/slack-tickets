import requests

from django.conf import settings


def call(method, data={}):
    url = "https://slack.com/api/{}".format(method)
    data['token'] = settings.DIEGO_SLACK_TEST_TOKEN
    response = requests.post(url, data=data)
    return response.json()
