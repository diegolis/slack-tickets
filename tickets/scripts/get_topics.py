import requests

from django.conf import settings
from django.contrib.auth.models import User

from tickets import slack
from tickets.models import Topic


def run():
    data = slack.call("channels.list")
    if not data['ok']:
        print(data['error'])
    for channel in data['channels']:
        topic, _ = Topic.objects.get_or_create(name=channel['name'])
        if not topic.slack_channel_id:
            topic.slack_channel_id = channel['id']
            topic.save()
        topic_data = slack.call("channels.info", data={'channel': topic.slack_channel_id})
        for user_id in topic_data['channel']['members']:
            topic.members.add(User.objects.get(profile__slack_user_id=user_id))
