from django.contrib import admin

from tickets.models import Ticket, Profile, Topic, Comment, CommentIntent


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'topic', 'priority', 'status',
        'author', 'assigned_to')
    search_fields = ('title', 'description',)
    list_filter = ['status', 'priority', 'assigned_to', 'topic']
    exclude = ["author", "status"]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'slack_user_id', 'slack_user_name')


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('name', 'slack_channel_id')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('text', 'user', 'topic', 'timestamp', 'slack_ts', 'thread_ts')
    list_filter = ('user', 'topic')


@admin.register(CommentIntent)
class CommentIntentAdmin(admin.ModelAdmin):
    list_display = ('user', 'topic', 'timestamp', 'ticket', 'pending')
    list_filter = ['pending']

